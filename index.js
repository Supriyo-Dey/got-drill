import got from "./data.js";

// 1. Write a function called `countAllPeople` which counts the 
// total number of people in `got` variable defined in `data.js` file.

function countAllPeople(data) {
    let totalCount = 0;

    for (let index = 0; index < data.houses.length; index++) {
        const house = data.houses[index];
        totalCount += house.people.length;
    }

    return totalCount;
}

console.log(countAllPeople(got));

// 2. Write a function called `peopleByHouses` which counts the 
// total number of people in different houses in the `got` variable defined in `data.js` file.
function peopleByHouses(data) {
    const perHouse = {};
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        perHouse[house.name] = house.people.length;
    }
    return perHouse;
}
console.log(peopleByHouses(got));

// 3. Write a function called `everyone` which returns a
// array of names of all the people in `got` variable.
function everyone(data) {
    const result = [];
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        for(let iter=0; iter<house.people.length;iter++){
            result.push(house.people[iter].name);
        }
    }
    return result;
}
console.log(everyone(got));

// 4. Write a function called `nameWithS` which returns a array 
// of names of all the people in `got` variable whose name includes `s` or `S`.
function namewithS(data) {
    const result = []
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        for(let iter=0; iter<house.people.length;iter++){
            let name = house.people[iter].name;
            if(name.toLowerCase().includes("s")){
                result.push(name);
            }
        }
    }
    return result;
}
console.log(namewithS(got));

// 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.
function namewithA(data) {
    const result = []
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        for(let iter=0; iter<house.people.length;iter++){
            let name = house.people[iter].name;
            if(name.toLowerCase().includes("a")){
                result.push(name);
            }
        }
    }
    return result;
}
console.log(namewithA(got));

//6. Write a function called `surnameWithS` which returns 
// a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).
function surnameWithS(data) {
    const result = []
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        for(let iter=0; iter<house.people.length;iter++){
            let name = house.people[iter].name;
            let surname = name.split(" ").pop();
            if(surname.startsWith("S")){
                result.push(name);
            }
        }
    }
    return result;
}
console.log(surnameWithS(got));

// 7. Write a function called `surnameWithA` which returns a array of names of all the people 
// in `got` variable whoes surname is starting with `A`(capital a).

function surnameWithA(data) {
    const result = []
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        for(let iter=0; iter<house.people.length;iter++){
            let name = house.people[iter].name;
            let surname = name.split(" ").pop();
            if(surname.startsWith("A")){
                result.push(name);
            }
        }
    }
    return result;
}
console.log("---7---")
console.log(surnameWithA(got));

// 8. Write a function called `peopleNameOfAllHouses` which returns an object 
// with the key of the name of house
// and value will be all the people in the house in an array.
function peopleNameOfAllHouses(data) {
    const result = {};
    for (let index = 0; index < data.houses.length; index++) {
        let house = data.houses[index];
        let names = [];
        for(let iter=0; iter<house.people.length;iter++){
            let name = house.people[iter].name;
            names.push(name);            
        }
        result[house.name] = names;
    }
    return result;
}
const result = peopleNameOfAllHouses(got);
console.log(result);