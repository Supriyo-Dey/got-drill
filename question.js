const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];

function names(items){
    if(Array.isArray(items)){
        const result = items.reduce((acc,cv) => {
            if(cv.contains.includes("Vitamin C")){
                acc.push(cv.name);
            }
            return acc;
        },[]);
        return result;
    }
};
const result = names(items);
console.log(result);